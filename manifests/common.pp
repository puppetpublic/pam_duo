# This class contains the common Puppet code needed to configure Duo/PAM 
# integration.  That includes package installation and wallet retrieval.

class pam_duo::common (
  $wallet_name
) {

  # Pull in Duo's PAM integration package
  package { 'libpam-duo': ensure => present }

  # Make sure sshd (at least version 6.2 is installed)
  case $::operatingsystem {
    'Debian': {
      $sshd_package = 'openssh-server'

      # Debian wheezy is the oldest supported version
      if ($::lsbmajdistrelease < 7) {
        fail('pam_duo requires at least Debian wheezy')
      }
      elsif ($::lsbmajdistrelease == 7) {
        # On wheezy, pin the backported openssh
        file {
          '/etc/apt/preferences.d/openssh':
            ensure => present,
            source => "puppet:///modules/pam_duo/etc/apt/preferences.d/openssh",
        }
      }
    }

    'Ubuntu': {
      $sshd_package = 'openssh-server'
      
      # Ubuntu trusty is the oldest supported version
      if ($::lsbmajdistrelease < 14) {
        fail('pam_duo requires at least Ubuntu trusty')
      }
    }

    # NOTE: The templates have not been fully-validated on RHEL-type systems,
    # so pam_duo will still fail on them (just not in this section)
    'RedHat', 'CentOS': {
      $sshd_package = 'openssh-server'

      # RHEL/CentOS 7 is the oldest supported version
      if ($::lsbmajdistrelease < 7) {
        fail('pam_duo requires at least RHEL/CentOS 7')
      }
    }

    default: {
      fail('pam_duo is not supported on this OS.')
    }
  }

  # Actually install the package with sshd now
  if !defined( Package[$sshd_package] ) {
    package { $sshd_package:
      ensure => 'installed',
    }
  } else {
    # On Debian/Ubuntu, manually run aptitude to upgrade openssh-server
    if ($::osfamily == 'Debian') {
      exec { 'update ssh':
        command     => 'aptitude -y install openssh-server',
        provider    => shell,
        refreshonly => true,
        require     => Exec['aptitude update'],
      }
    }
  }

  # Install the duo configuration.  The object is not written to the
  # default loaction because base::wallet will not over write the
  # configuration file supplied with the package install.
  $wallet_name_downcase = downcase($wallet_name)
  base::wallet { $wallet_name_downcase:
    ensure  => present,
    type    => 'duo-pam',
    path    => '/etc/security/pam_duo_su.conf',
    require => Package['libpam-duo'],
  }
}
