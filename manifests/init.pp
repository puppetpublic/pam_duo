# Generate the configuration to enforce the following policies:
#
#  * Require GSS-API for remote root logins.
#  * Require GSS-API or password for normal user, i.e. non root, logins.
#  * Require Duo two step authentication for normal users.
#
# While the define does accept the fdqn for the host it is not
# expected that this will ever need to be specified.
#
# Examples
#
# The simplest way to use the pam_duo class is to just include it.
# This pulls the DUO integration down using wallet and associated
# with the host.
#
#  include pam_duo
#
# For clusters creating a DUO integration for each host is over kill.
# The following example specifies a cluster name for the wallet
# object and allows ssh connections to ports 22 and 44.
#
#  class { 'pam_duo':
#    port        => ['22', '44'],
#    wallet_name => 'host/cluster.stanford.edu',
#  }

class pam_duo (
  $port        = ['22'],
  $wallet_name = $::fqdn
) {
  class { 'pam_duo::ssh': port => $port }

  # Bring in common pam_duo configuration
  class { 'pam_duo::common': wallet_name => $wallet_name }
  contain pam_duo::common

  # Modify the sshd configuration to require both a GSS-API and a PAM
  # authentication.
  file {'/etc/pam.d/sshd':
    ensure => present,
    source => "puppet:///modules/pam_duo/etc/pam.d/sshd_both",
  }
}

# Class to override the base::ssh configuration.
class pam_duo::ssh ( $port ) inherits base::ssh {
  # TODO: use template for configuring sshd for RHEL as well as
  # Debian/Ubuntu This probably breaks rhel5 (and 6?)
  if $::operatingsystem =~ /^(RedHat|CentOS)$/ and $::lsbmajdistrelease < 6 {
    fail ("pam_duo doesn't support this platform yet.")
  }
  Base::Ssh::Config::Sshd['/etc/ssh/sshd_config'] {
    content => template('pam_duo/etc/ssh/sshd_config_both'),
  }
}
