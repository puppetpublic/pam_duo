# Generate the configuration to enforce the following policies:
#
#  * Allow username/password or GSS-API authentication for normal users.
#  * Require Duo two step authentication for normal users.
#  * Require GSS-API for remote root logins without two step.
#
# Simple example
# --------------
#
#     class { 'pam_duo::password': }
#
# Bastion host example
# --------------------
#
#     class { 'pam_duo::password': port => '22,44' }

class pam_duo::password (
  $port        = ['22', '44'],
  $wallet_name = $::fqdn
) {
  class { 'pam_duo::password::ssh': port => $port }

  # Bring in common pam_duo configuration
  class { 'pam_duo::common': wallet_name => $wallet_name }
  contain pam_duo::common

  # Modify the sshd configuration to allow GSS-API or PAM
  # authentication.
  file {'/etc/pam.d/sshd':
    ensure => present,
    source => "puppet:///modules/pam_duo/etc/pam.d/sshd_password",
  }
}

# Class to override the base::ssh configuration.
class pam_duo::password::ssh ( $port ) inherits base::ssh {
  # TODO: use template for configuring sshd for RHEL as well as
  # Debian/Ubuntu This probably breaks rhel5 (and 6?)
  if $::operatingsystem =~ /^(RedHat|CentOS)$/ and $::lsbmajdistrelease < 6 {
    fail ("pam_duo doesn't support this platform yet.")
  }
  Base::Ssh::Config::Sshd['/etc/ssh/sshd_config'] {
    content => template('pam_duo/etc/ssh/sshd_config_password'),
  }
}
